void main() {
  ///jadi intinya adalah
  ///perbedaan final dan const adalah jika kita menggunakan final itu berarti
  ///kita dapat memberi value dari sebuah variable nanti atau dimasa yang akan datang
  ///berbeda dengan const yang kebalikan dari tipe data 'final'
  ///namun pada dasarnya kedua tipe data ini sama-sama berfungsi untuk membuat value tidak dapat dirubah
  final String name;
  name = 'joko';
  print(name);

  const String firstName = 'kevin';
  print(firstName);
}
